import 'package:chuatjaclone/core/error/failure.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  test('should return true when comparing identical object attributes', () {
    final failure1 = FailureTest();
    final failure2 = FailureTest();

    expect(failure1.props, failure2.props);
  });
}

class FailureTest extends Failure {}
