import 'package:chuatjaclone/core/usecase/usecase.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  test('should return true when comparing identical object attributes', () {
    final usecase1 = NoParams();
    final usecase2 = NoParams();

    expect(usecase1.props, usecase2.props);
  });
}
