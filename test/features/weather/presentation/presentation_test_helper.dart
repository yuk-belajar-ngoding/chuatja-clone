import 'package:chuatjaclone/features/weather/domain/entities/location.dart';
import 'package:chuatjaclone/features/weather/domain/entities/weather.dart';
import 'package:chuatjaclone/features/weather/domain/entities/weather_current.dart';
import 'package:chuatjaclone/features/weather/domain/entities/weather_forecast.dart';
import 'package:chuatjaclone/features/weather/domain/entities/weather_forecast_astro.dart';
import 'package:chuatjaclone/features/weather/domain/entities/weather_forecast_hour.dart';

const tLocation = Location(
  locationName: 'Jakarta',
  locationLatitude: -6.21,
  locationLongitude: 106.85,
);
const tWeatherCurrent = WeatherCurrent(
  lastUpdatedTime: "2023-11-23 01:45",
  temperatureCelsius: 27.0,
  isDay: 0,
  conditionText: "Partly cloudy",
  conditionIcon: "{apiCDNUrl}weather/64x64/night/116.png",
);
const tWeatherForecastAstro = WeatherForecastAstro(
  astroSunriseTime: "05:26 AM",
  astroSunsetTime: "05:52 PM",
  astroMoonriseTime: "02:18 PM",
  astroMoonsetTime: "01:53 AM",
);
const tWeatherForecastHour = WeatherForecastHour(
  timeEpoch: 1700672400,
  temperatureCelsius: 29.2,
  conditionIcon: "{apiCDNUrl}weather/64x64/night/113.png",
  windDirection: "WSW",
  humidity: 69,
  uvIndex: 1.0,
);
const tWeatherForecast = WeatherForecast(
  astro: tWeatherForecastAstro,
  hour: tWeatherForecastHour,
);
const tWeather = Weather(
  location: tLocation,
  weatherCurrent: tWeatherCurrent,
  weatherForecast: tWeatherForecast,
);
