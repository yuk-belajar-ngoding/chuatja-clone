import 'package:chuatjaclone/core/error/failure.dart';
import 'package:chuatjaclone/features/weather/domain/usecases/get_weather_usecase.dart';
import 'package:chuatjaclone/features/weather/presentation/bloc/get_weather/get_weather_bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';

import '../../presentation_test_helper.dart';

@GenerateNiceMocks([MockSpec<GetWeatherUsecase>()])
import 'get_weather_bloc_test.mocks.dart';

void main() {
  test(
      'should return true when comparing bloc event identical object attributes',
      () {
    final initEvent1 = GetWeatherInit();
    final initEvent2 = GetWeatherInit();
    expect(initEvent1.props, initEvent2.props);

    const weatherEvent1 = GetWeather();
    const weatherEvent2 = GetWeather();
    expect(weatherEvent1.props, weatherEvent2.props);
  });

  test(
      'should return true when comparing bloc state identical object attributes',
      () {
    final initState1 = GetWeatherInitializing();
    final initState2 = GetWeatherInitializing();
    expect(initState1.props, initState2.props);

    final loadingState1 = GetWeatherLoading();
    final loadingState2 = GetWeatherLoading();
    expect(loadingState1.props, loadingState2.props);

    const loadedState1 = GetWeatherLoaded(weather: tWeather);
    const loadedState2 = GetWeatherLoaded(weather: tWeather);
    expect(loadedState1.props, loadedState2.props);
  });

  late GetWeatherBloc getWeatherBloc;
  late MockGetWeatherUsecase mockGetWeatherUsecase;

  setUp(() {
    mockGetWeatherUsecase = MockGetWeatherUsecase();
    getWeatherBloc = GetWeatherBloc(
      getWeatherUsecase: mockGetWeatherUsecase,
    );
  });

  test('should return initial state as per started', () {
    // assert
    expect(getWeatherBloc.state, equals(GetWeatherInitializing()));
  });

  test('should return loading state when initiating bloc', () {
    // expect later
    final expectedStates = [GetWeatherLoading()];
    expectLater(getWeatherBloc.stream, emitsInOrder(expectedStates));

    // act
    getWeatherBloc.add(GetWeatherInit());
  });

  void setUpMockGetWeatherSuccess() {
    when(mockGetWeatherUsecase(any)).thenAnswer((_) async {
      return const Right(tWeather);
    });
  }

  test('should get data from the get weather usecase', () async {
    // arrange
    setUpMockGetWeatherSuccess();

    // act
    getWeatherBloc.add(const GetWeather());
    await untilCalled(mockGetWeatherUsecase(any));

    // assert
    verify(mockGetWeatherUsecase(any));
  });

  test(
    'should emit [Loaded] when data is gotten successfully',
    () async {
      // arrange
      setUpMockGetWeatherSuccess();

      // assert later
      final expectedStates = [
        const GetWeatherLoaded(weather: tWeather),
      ];
      expectLater(getWeatherBloc.stream, emitsInOrder(expectedStates));

      // act
      getWeatherBloc.add(const GetWeather());
    },
  );

  test(
    '''should emit [Error] with a proper message
    for the error when getting data fails''',
    () async {
      // arrange
      when(mockGetWeatherUsecase(any)).thenAnswer((_) async {
        return Left(ServerFailure());
      });

      // assert later
      final expectedStates = [
        const GetWeatherError(message: defaultFailureMessage),
      ];
      expectLater(getWeatherBloc.stream, emitsInOrder(expectedStates));

      // act
      getWeatherBloc.add(const GetWeather());
    },
  );
}
