import 'dart:convert';

import 'package:chuatjaclone/core/error/exception.dart';
import 'package:chuatjaclone/features/weather/data/sources/weather_datasource.dart';
import 'package:chuatjaclone/features/weather/domain/usecases/get_weather_usecase.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:http/http.dart' as http;
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';

import '../../../../fixtures/fixture_reader.dart';

@GenerateNiceMocks([MockSpec<http.Client>()])
import 'weather_datasource_test.mocks.dart';

void main() {
  late WeatherDataSourceImpl weatherDataSource;
  late MockClient mockHttpClient;

  setUp(() {
    mockHttpClient = MockClient();
    weatherDataSource = WeatherDataSourceImpl(client: mockHttpClient);
  });

  final endpoint = WeatherDataSourceImpl.endpoint['getWeatherForecast'];
  final tJson = fixture('weather_forecast_by_location.json');

  String tParamsKeyword = 'Jakarta';
  WeatherParams tParams = WeatherParams(searchKeyword: tParamsKeyword);

  void setUpMockFetchApiSuccess() {
    when(mockHttpClient.get(any)).thenAnswer((_) async {
      return http.Response(tJson, 200);
    });
  }

  test(
    'should perform a GET request on an URL with "forecast" being the endpoint',
    () async {
      setUpMockFetchApiSuccess();

      weatherDataSource.getWeather(tParams);

      final queryString =
          weatherDataSource.getQueryString(tParams.searchKeyword);
      final endpointUrl = apiBaseUrl + endpoint! + queryString;
      final Uri uri = Uri.parse(endpointUrl);
      verify(mockHttpClient.get(uri));
    },
  );

  test(
    'should get weather info from "forecast" endpoints',
    () async {
      setUpMockFetchApiSuccess();
      var tApiCDNUrl = apiCDNUrl;
      tApiCDNUrl = '{apiCDNUrl}';
      final tResult = json.decode(tJson);
      final tWeatherModel = weatherDataSource.transformJsonToModel(tResult);

      final result =
          await weatherDataSource.fetchWeatherForecast(tParams.searchKeyword);
      final iconUrl = result.weatherCurrent.conditionIcon;
      final bool isValidUrl = iconUrl.contains(tApiCDNUrl);

      expect(result, tWeatherModel);
      expect(iconUrl, tWeatherModel.weatherCurrent.conditionIcon);
      expect(isValidUrl, true);
    },
  );

  test(
    'should throw a ServerException when the response code is not 200',
    () async {
      when(mockHttpClient.get(any)).thenAnswer((_) async {
        return http.Response('Something went wrong', 404);
      });

      final call = weatherDataSource.getWeather;

      expect(
        () => call(tParams),
        throwsA(const TypeMatcher<ServerException>()),
      );
    },
  );
}
