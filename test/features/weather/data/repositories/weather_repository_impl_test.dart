import 'package:chuatjaclone/core/error/exception.dart';
import 'package:chuatjaclone/core/error/failure.dart';
import 'package:chuatjaclone/features/weather/data/models/weather_model.dart';
import 'package:chuatjaclone/features/weather/data/repositories/weather_repository_impl.dart';
import 'package:chuatjaclone/features/weather/data/sources/weather_datasource.dart';
import 'package:chuatjaclone/features/weather/domain/entities/weather.dart';
import 'package:chuatjaclone/features/weather/domain/usecases/get_weather_usecase.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

import '../../domain/domain_test_helper.dart';
import '../data_test_helper.dart';

class MockWeatherDataSource extends Mock implements WeatherDataSource {}

void main() {
  late WeatherRepositoryImpl repository;
  late MockWeatherDataSource mockWeatherDataSource;

  setUp(() {
    mockWeatherDataSource = MockWeatherDataSource();

    repository = WeatherRepositoryImpl(
      dataSource: mockWeatherDataSource,
    );
  });

  const tWeatherModel = WeatherModel(
    location: tLocationModel,
    weatherCurrent: tWeatherCurrentModel,
    weatherForecast: tWeatherForecastModel,
  );

  Weather tWeather = tWeatherModel;
  WeatherParams tParams = WeatherParams(searchKeyword: tLocation.locationName);

  test(
    'should return data when the remote API call is successful',
    () async {
      // arrange
      when(mockWeatherDataSource.getWeather(tParams)).thenAnswer((_) async {
        return tWeatherModel;
      });

      // act
      final result = await repository.getWeather(tParams);
      final Weather? resultWeather = result!.getOrElse(() => null);

      // assert
      verify(mockWeatherDataSource.getWeather(tParams));
      expect(resultWeather, tWeather);
    },
  );

  test(
    'should return server failure when the remote API call is unsuccessful',
    () async {
      when(mockWeatherDataSource.getWeather(tParams))
          .thenThrow(ServerException());

      final result = await repository.getWeather(tParams);
      Either<Failure, Weather?>? serverFailure = result;

      verify(mockWeatherDataSource.getWeather(tParams));
      expect(result, serverFailure);
    },
  );
}
