import 'dart:convert';

import 'package:chuatjaclone/features/weather/data/models/weather_current_model.dart';
import 'package:chuatjaclone/features/weather/domain/entities/weather_current.dart';
import 'package:flutter_test/flutter_test.dart';

import '../../../../fixtures/fixture_reader.dart';
import '../data_test_helper.dart';

void main() {
  test('should be a subclass of WeatherCurrent entity', () {
    TypeMatcher isAWeatherCurrent = isA<WeatherCurrent>();

    expect(tWeatherCurrentModel, isAWeatherCurrent);
  });

  test('''should return a valid model 
      when the JSON contains weather current fields''', () async {
    final Map<String, dynamic> jsonMap =
        await json.decode(fixture('weather_current_by_location.json'));

    final result = WeatherCurrentModel.fromJson(jsonMap);

    expect(result.lastUpdatedTime, tWeatherCurrentModel.lastUpdatedTime);
    expect(result.temperatureCelsius, tWeatherCurrentModel.temperatureCelsius);
    expect(result.isDay, tWeatherCurrentModel.isDay);
    expect(result.conditionText, tWeatherCurrentModel.conditionText);
    expect(result.conditionIcon, tWeatherCurrentModel.conditionIcon);
  });
}
