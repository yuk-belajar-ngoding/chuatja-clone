import 'package:chuatjaclone/features/weather/domain/entities/weather_forecast.dart';
import 'package:flutter_test/flutter_test.dart';

import '../data_test_helper.dart';

void main() {
  test('should be a subclass of WeatherForecast entity', () {
    TypeMatcher isAWeatherForecast = isA<WeatherForecast>();

    expect(tWeatherForecastModel, isAWeatherForecast);
  });
}
