import 'dart:convert';

import 'package:chuatjaclone/features/weather/data/models/location_model.dart';
import 'package:chuatjaclone/features/weather/domain/entities/location.dart';
import 'package:flutter_test/flutter_test.dart';

import '../../../../fixtures/fixture_reader.dart';
import '../data_test_helper.dart';

void main() {
  test('should be a subclass of Location entity', () {
    TypeMatcher isALocation = isA<Location>();

    expect(tLocationModel, isALocation);
  });

  test('should return a valid model when the JSON contains location fields',
      () async {
    final Map<String, dynamic> jsonMap =
        await json.decode(fixture('weather_current_by_location.json'));

    final result = LocationModel.fromJson(jsonMap);

    expect(result.locationName, tLocationModel.locationName);
    expect(result.locationLatitude, tLocationModel.locationLatitude);
    expect(result.locationLongitude, tLocationModel.locationLongitude);
  });
}
