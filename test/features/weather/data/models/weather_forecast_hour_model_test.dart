import 'dart:convert';

import 'package:chuatjaclone/features/weather/data/models/weather_forecast_hour_model.dart';
import 'package:chuatjaclone/features/weather/domain/entities/weather_forecast_hour.dart';
import 'package:flutter_test/flutter_test.dart';

import '../../../../fixtures/fixture_reader.dart';
import '../data_test_helper.dart';

void main() {
  test('should be a subclass of WeatherForecastHour entity', () {
    TypeMatcher isAWeatherForecastHour = isA<WeatherForecastHour>();

    expect(tWeatherForecastHourModel, isAWeatherForecastHour);
  });

  test('''should return a valid model 
      when the JSON contains weather forecast hour fields''', () async {
    final Map<String, dynamic> jsonMap =
        await json.decode(fixture('weather_forecast_by_location.json'));

    final result = WeatherForecastHourModel.fromJson(jsonMap);

    expect(result.timeEpoch, tWeatherForecastHourModel.timeEpoch);
    expect(result.temperatureCelsius,
        tWeatherForecastHourModel.temperatureCelsius);
    expect(result.conditionIcon, tWeatherForecastHourModel.conditionIcon);
    expect(result.windDirection, tWeatherForecastHourModel.windDirection);
    expect(result.humidity, tWeatherForecastHourModel.humidity);
    expect(result.uvIndex, tWeatherForecastHourModel.uvIndex);
  });
}
