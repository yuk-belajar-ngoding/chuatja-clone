import 'dart:convert';

import 'package:chuatjaclone/features/weather/data/models/weather_forecast_astro_model.dart';
import 'package:chuatjaclone/features/weather/domain/entities/weather_forecast_astro.dart';
import 'package:flutter_test/flutter_test.dart';

import '../../../../fixtures/fixture_reader.dart';
import '../data_test_helper.dart';

void main() {
  test('should be a subclass of WeatherForecastAstro entity', () {
    TypeMatcher isAWeatherForecastAstro = isA<WeatherForecastAstro>();

    expect(tWeatherForecastAstroModel, isAWeatherForecastAstro);
  });

  test('''should return a valid model 
      when the JSON contains weather forecast astro fields''', () async {
    final Map<String, dynamic> jsonMap =
        await json.decode(fixture('weather_forecast_by_location.json'));

    final result = WeatherForecastAstroModel.fromJson(jsonMap);

    expect(
        result.astroSunriseTime, tWeatherForecastAstroModel.astroSunriseTime);
    expect(result.astroSunsetTime, tWeatherForecastAstroModel.astroSunsetTime);
    expect(
        result.astroMoonriseTime, tWeatherForecastAstroModel.astroMoonriseTime);
    expect(
        result.astroMoonsetTime, tWeatherForecastAstroModel.astroMoonsetTime);
  });
}
