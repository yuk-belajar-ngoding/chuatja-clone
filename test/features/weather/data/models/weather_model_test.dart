import 'package:chuatjaclone/features/weather/data/models/weather_model.dart';
import 'package:chuatjaclone/features/weather/domain/entities/weather.dart';
import 'package:flutter_test/flutter_test.dart';

import '../data_test_helper.dart';

void main() {
  test('should be a subclass of Weather entity', () {
    const tWeatherModel = WeatherModel(
      location: tLocationModel,
      weatherCurrent: tWeatherCurrentModel,
      weatherForecast: tWeatherForecastModel,
    );

    TypeMatcher isAWeather = isA<Weather>();

    expect(tWeatherModel, isAWeather);
  });
}
