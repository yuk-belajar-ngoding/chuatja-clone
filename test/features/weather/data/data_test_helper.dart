import 'package:chuatjaclone/features/weather/data/models/location_model.dart';
import 'package:chuatjaclone/features/weather/data/models/weather_current_model.dart';
import 'package:chuatjaclone/features/weather/data/models/weather_forecast_astro_model.dart';
import 'package:chuatjaclone/features/weather/data/models/weather_forecast_hour_model.dart';
import 'package:chuatjaclone/features/weather/data/models/weather_forecast_model.dart';

const tLocationModel = LocationModel(
  locationName: 'Jakarta',
  locationLatitude: -6.21,
  locationLongitude: 106.85,
);

const tWeatherCurrentModel = WeatherCurrentModel(
  lastUpdatedTime: "2023-11-23 01:45",
  temperatureCelsius: 27.0,
  isDay: 0,
  conditionText: "Partly cloudy",
  conditionIcon: "{apiCDNUrl}weather/64x64/night/116.png",
);

const tWeatherForecastAstroModel = WeatherForecastAstroModel(
  astroSunriseTime: "05:26 AM",
  astroSunsetTime: "05:52 PM",
  astroMoonriseTime: "02:18 PM",
  astroMoonsetTime: "01:53 AM",
);

const tWeatherForecastHourModel = WeatherForecastHourModel(
  timeEpoch: 1700672400,
  temperatureCelsius: 29.2,
  conditionIcon: "{apiCDNUrl}weather/64x64/night/113.png",
  windDirection: "WSW",
  humidity: 69,
  uvIndex: 1.0,
);

const tWeatherForecastModel = WeatherForecastModel(
  astro: tWeatherForecastAstroModel,
  hour: tWeatherForecastHourModel,
);
