import 'package:chuatjaclone/features/weather/domain/entities/location.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  test('should return true when comparing identical object attributes', () {
    const location1 = Location(
      locationName: 'Sample City',
      locationLatitude: -1.0,
      locationLongitude: 1.0,
    );
    const location2 = Location(
      locationName: 'Sample City',
      locationLatitude: -1.0,
      locationLongitude: 1.0,
    );

    expect(location1.props, location2.props);
  });
}
