import 'package:chuatjaclone/features/weather/domain/entities/weather.dart';
import 'package:flutter_test/flutter_test.dart';

import '../domain_test_helper.dart';

void main() {
  test('should return true when comparing identical object attributes', () {
    const weather1 = Weather(
      location: tLocation,
      weatherCurrent: tWeatherCurrent,
      weatherForecast: tWeatherForecast,
    );
    const weather2 = Weather(
      location: tLocation,
      weatherCurrent: tWeatherCurrent,
      weatherForecast: tWeatherForecast,
    );

    expect(weather1.props, weather2.props);
  });
}
