import 'package:chuatjaclone/features/weather/domain/entities/weather_forecast_hour.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  test('should return true when comparing identical object attributes', () {
    const weatherForecastHour1 = WeatherForecastHour(
      timeEpoch: 1700629200,
      temperatureCelsius: 20.0,
      conditionIcon: "ClearIcon",
      windDirection: "N",
      humidity: 1,
      uvIndex: 1.0,
    );
    const weatherForecastHour2 = WeatherForecastHour(
      timeEpoch: 1700629200,
      temperatureCelsius: 20.0,
      conditionIcon: "ClearIcon",
      windDirection: "N",
      humidity: 1,
      uvIndex: 1.0,
    );

    expect(weatherForecastHour1.props, weatherForecastHour2.props);
  });
}
