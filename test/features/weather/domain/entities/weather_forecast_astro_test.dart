import 'package:chuatjaclone/features/weather/domain/entities/weather_forecast_astro.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  test('should return true when comparing identical object attributes', () {
    const weatherForecastAstro1 = WeatherForecastAstro(
      astroSunriseTime: "2023-11-22 00:01",
      astroSunsetTime: "2023-11-22 00:02",
      astroMoonriseTime: "2023-11-22 00:03",
      astroMoonsetTime: "2023-11-22 00:04",
    );
    const weatherForecastAstro2 = WeatherForecastAstro(
      astroSunriseTime: "2023-11-22 00:01",
      astroSunsetTime: "2023-11-22 00:02",
      astroMoonriseTime: "2023-11-22 00:03",
      astroMoonsetTime: "2023-11-22 00:04",
    );

    expect(weatherForecastAstro1.props, weatherForecastAstro2.props);
  });
}
