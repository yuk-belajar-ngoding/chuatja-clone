import 'package:chuatjaclone/features/weather/domain/entities/weather_current.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  test('should return true when comparing identical object attributes', () {
    const weatherCurrent1 = WeatherCurrent(
      lastUpdatedTime: "2023-11-22 00:00",
      temperatureCelsius: 27.0,
      isDay: 1,
      conditionText: "Clear",
      conditionIcon: "ClearIcon",
    );
    const weatherCurrent2 = WeatherCurrent(
      lastUpdatedTime: "2023-11-22 00:00",
      temperatureCelsius: 27.0,
      isDay: 1,
      conditionText: "Clear",
      conditionIcon: "ClearIcon",
    );

    expect(weatherCurrent1.props, weatherCurrent2.props);
  });
}
