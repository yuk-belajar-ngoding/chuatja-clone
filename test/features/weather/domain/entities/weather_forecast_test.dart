import 'package:chuatjaclone/features/weather/domain/entities/weather_forecast.dart';
import 'package:chuatjaclone/features/weather/domain/entities/weather_forecast_astro.dart';
import 'package:chuatjaclone/features/weather/domain/entities/weather_forecast_hour.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  test('should return true when comparing identical object attributes', () {
    const weatherForecastAstro = WeatherForecastAstro(
      astroSunriseTime: "2023-11-22 00:01",
      astroSunsetTime: "2023-11-22 00:02",
      astroMoonriseTime: "2023-11-22 00:03",
      astroMoonsetTime: "2023-11-22 00:04",
    );
    const weatherForecastHour = WeatherForecastHour(
      timeEpoch: 1700629200,
      temperatureCelsius: 20.0,
      conditionIcon: "ClearIcon",
      windDirection: "N",
      humidity: 1,
      uvIndex: 1.0,
    );
    const weatherForecast1 = WeatherForecast(
      astro: weatherForecastAstro,
      hour: weatherForecastHour,
    );
    const weatherForecast2 = WeatherForecast(
      astro: weatherForecastAstro,
      hour: weatherForecastHour,
    );

    expect(weatherForecast1.props, weatherForecast2.props);
  });
}
