import 'package:chuatjaclone/features/weather/domain/entities/weather.dart';
import 'package:chuatjaclone/features/weather/domain/repositories/weather_repository.dart';
import 'package:chuatjaclone/features/weather/domain/usecases/get_weather_usecase.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

import '../domain_test_helper.dart';

class MockWeatherRepository extends Mock implements WeatherRepository {}

void main() {
  late MockWeatherRepository mockWeatherRepository;
  late GetWeatherUsecase usecase;

  const tWeather = Weather(
    location: tLocation,
    weatherCurrent: tWeatherCurrent,
    weatherForecast: tWeatherForecast,
  );

  setUp(() {
    mockWeatherRepository = MockWeatherRepository();
    usecase = GetWeatherUsecase(mockWeatherRepository);
  });

  WeatherParams tParams = WeatherParams(searchKeyword: tLocation.locationName);
  test('should get weather info from repository', () async {
    // arrange
    when(mockWeatherRepository.getWeather(tParams))
        .thenAnswer((_) async => const Right(tWeather));

    // act
    final result = await usecase(tParams);

    // assert
    expect(result, const Right(tWeather));
    verify(mockWeatherRepository.getWeather(tParams));
    verifyNoMoreInteractions(mockWeatherRepository);
  });
}
