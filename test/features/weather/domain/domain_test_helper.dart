import 'package:chuatjaclone/features/weather/domain/entities/location.dart';
import 'package:chuatjaclone/features/weather/domain/entities/weather_current.dart';
import 'package:chuatjaclone/features/weather/domain/entities/weather_forecast.dart';
import 'package:chuatjaclone/features/weather/domain/entities/weather_forecast_astro.dart';
import 'package:chuatjaclone/features/weather/domain/entities/weather_forecast_hour.dart';

const tLocation = Location(
  locationName: 'Sample City',
  locationLatitude: -1.0,
  locationLongitude: 1.0,
);
const tWeatherCurrent = WeatherCurrent(
  lastUpdatedTime: "2023-11-22 00:00",
  temperatureCelsius: 27.0,
  isDay: 1,
  conditionText: "Clear",
  conditionIcon: "ClearIcon",
);
const tWeatherForecastAstro = WeatherForecastAstro(
  astroSunriseTime: "2023-11-22 00:01",
  astroSunsetTime: "2023-11-22 00:02",
  astroMoonriseTime: "2023-11-22 00:03",
  astroMoonsetTime: "2023-11-22 00:04",
);
const tWeatherForecastHour = WeatherForecastHour(
  timeEpoch: 1700629200,
  temperatureCelsius: 20.0,
  conditionIcon: "ClearIcon",
  windDirection: "N",
  humidity: 1,
  uvIndex: 1.0,
);
const tWeatherForecast = WeatherForecast(
  astro: tWeatherForecastAstro,
  hour: tWeatherForecastHour,
);
