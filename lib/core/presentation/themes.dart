import 'package:flex_color_scheme/flex_color_scheme.dart';
import 'package:flutter/material.dart';

class AppThemes {
  static final ValueNotifier<ThemeMode> themeNotifier =
      ValueNotifier(ThemeMode.system);

  static final lightTheme =
      FlexColorScheme.light(scheme: FlexScheme.brandBlue).toTheme;
  static final darkTheme =
      FlexColorScheme.dark(scheme: FlexScheme.brandBlue).toTheme;
}
