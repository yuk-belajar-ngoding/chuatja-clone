import 'package:chuatjaclone/core/presentation/themes.dart';
import 'package:chuatjaclone/features/weather/presentation/screens/home_screen.dart';
import 'package:flutter/material.dart';

class ChuatjaApp extends StatelessWidget {
  const ChuatjaApp({super.key});

  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder<ThemeMode>(
      valueListenable: AppThemes.themeNotifier,
      builder: (_, ThemeMode currentMode, __) {
        return MaterialApp(
          debugShowCheckedModeBanner: false,
          theme: AppThemes.lightTheme,
          darkTheme: AppThemes.darkTheme,
          themeMode: currentMode,
          home: const HomeScreen(),
        );
      },
    );
  }
}
