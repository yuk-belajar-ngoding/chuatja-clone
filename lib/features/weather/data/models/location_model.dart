import 'package:chuatjaclone/features/weather/domain/entities/location.dart';

class LocationModel extends Location {
  const LocationModel({
    required super.locationName,
    required super.locationLatitude,
    required super.locationLongitude,
  });

  factory LocationModel.fromJson(Map<String, dynamic> json) {
    return LocationModel(
      locationName: json['location'] != null ? json['location']['name'] : '',
      locationLatitude:
          json['location'] != null ? json['location']['lat'] : 0.0,
      locationLongitude:
          json['location'] != null ? json['location']['lon'] : 0.0,
    );
  }
}
