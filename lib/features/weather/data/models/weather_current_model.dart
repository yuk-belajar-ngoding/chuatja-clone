import 'package:chuatjaclone/features/weather/domain/entities/weather_current.dart';

class WeatherCurrentModel extends WeatherCurrent {
  const WeatherCurrentModel({
    required super.lastUpdatedTime,
    required super.temperatureCelsius,
    required super.isDay,
    required super.conditionText,
    required super.conditionIcon,
  });

  factory WeatherCurrentModel.fromJson(Map<String, dynamic> json) {
    return WeatherCurrentModel(
      lastUpdatedTime:
          json['current'] != null ? json['current']['last_updated'] : '',
      temperatureCelsius:
          json['current'] != null ? json['current']['temp_c'] : 0.0,
      isDay: json['current'] != null ? json['current']['is_day'] : 0,
      conditionText:
          json['current'] != null ? json['current']['condition']['text'] : '',
      conditionIcon:
          json['current'] != null ? json['current']['condition']['icon'] : '',
    );
  }
}
