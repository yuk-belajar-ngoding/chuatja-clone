import 'package:chuatjaclone/features/weather/domain/entities/weather.dart';

class WeatherModel extends Weather {
  const WeatherModel({
    required super.location,
    required super.weatherCurrent,
    required super.weatherForecast,
  });
}
