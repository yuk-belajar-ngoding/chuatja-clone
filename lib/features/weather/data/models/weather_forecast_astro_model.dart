import 'package:chuatjaclone/features/weather/domain/entities/weather_forecast_astro.dart';

class WeatherForecastAstroModel extends WeatherForecastAstro {
  const WeatherForecastAstroModel({
    required super.astroSunriseTime,
    required super.astroSunsetTime,
    required super.astroMoonriseTime,
    required super.astroMoonsetTime,
  });

  factory WeatherForecastAstroModel.fromJson(Map<String, dynamic> json,
      [int dayIndex = 0]) {
    return WeatherForecastAstroModel(
      astroSunriseTime: json['forecast'] != null
          ? json['forecast']['forecastday'][dayIndex]['astro']['sunrise']
          : '',
      astroSunsetTime: json['forecast'] != null
          ? json['forecast']['forecastday'][dayIndex]['astro']['sunset']
          : '',
      astroMoonriseTime: json['forecast'] != null
          ? json['forecast']['forecastday'][dayIndex]['astro']['moonrise']
          : '',
      astroMoonsetTime: json['forecast'] != null
          ? json['forecast']['forecastday'][dayIndex]['astro']['moonset']
          : '',
    );
  }
}
