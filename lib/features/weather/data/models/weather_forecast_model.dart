import 'package:chuatjaclone/features/weather/domain/entities/weather_forecast.dart';

class WeatherForecastModel extends WeatherForecast {
  const WeatherForecastModel({
    required super.astro,
    required super.hour,
  });
}
