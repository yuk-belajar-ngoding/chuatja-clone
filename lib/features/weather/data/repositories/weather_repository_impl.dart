import 'package:chuatjaclone/core/error/exception.dart';
import 'package:chuatjaclone/core/error/failure.dart';
import 'package:chuatjaclone/features/weather/data/sources/weather_datasource.dart';
import 'package:chuatjaclone/features/weather/domain/entities/weather.dart';
import 'package:chuatjaclone/features/weather/domain/repositories/weather_repository.dart';
import 'package:chuatjaclone/features/weather/domain/usecases/get_weather_usecase.dart';
import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';

@LazySingleton(as: WeatherRepository)
class WeatherRepositoryImpl implements WeatherRepository {
  final WeatherDataSource dataSource;

  WeatherRepositoryImpl({required this.dataSource});

  @override
  Future<Either<Failure, Weather?>>? getWeather(WeatherParams params) async {
    try {
      final weatherData = await dataSource.getWeather(params);
      return Right(weatherData);
    } on ServerException {
      return Left(ServerFailure());
    }
  }
}
