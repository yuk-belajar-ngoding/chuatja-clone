import 'package:chuatjaclone/core/error/failure.dart';
import 'package:chuatjaclone/features/weather/domain/entities/weather.dart';
import 'package:chuatjaclone/features/weather/domain/usecases/get_weather_usecase.dart';
import 'package:dartz/dartz.dart';

abstract class WeatherRepository {
  Future<Either<Failure, Weather?>>? getWeather(WeatherParams params);
}
