import 'package:chuatjaclone/core/error/failure.dart';
import 'package:chuatjaclone/core/usecase/usecase.dart';
import 'package:chuatjaclone/features/weather/domain/entities/weather.dart';
import 'package:chuatjaclone/features/weather/domain/repositories/weather_repository.dart';
import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';

@lazySingleton
class GetWeatherUsecase extends UseCase<Weather?, WeatherParams> {
  final WeatherRepository repository;

  GetWeatherUsecase(this.repository);

  @override
  Future<Either<Failure, Weather?>>? call(WeatherParams params) async {
    return await repository.getWeather(params)!;
  }
}

class WeatherParams {
  dynamic searchKeyword;
  static const String defaultKeyword = 'Jakarta';

  WeatherParams({this.searchKeyword = defaultKeyword});
}
