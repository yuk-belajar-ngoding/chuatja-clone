import 'package:chuatjaclone/features/weather/domain/entities/location.dart';
import 'package:chuatjaclone/features/weather/domain/entities/weather_current.dart';
import 'package:chuatjaclone/features/weather/domain/entities/weather_forecast.dart';
import 'package:equatable/equatable.dart';

class Weather extends Equatable {
  final Location location;
  final WeatherCurrent weatherCurrent;
  final WeatherForecast weatherForecast;

  const Weather({
    required this.location,
    required this.weatherCurrent,
    required this.weatherForecast,
  });

  @override
  List<Object?> get props => [location, weatherCurrent, weatherForecast];
}
