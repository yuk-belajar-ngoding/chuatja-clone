import 'package:equatable/equatable.dart';

class Location extends Equatable {
  final String locationName;
  final double locationLatitude;
  final double locationLongitude;

  const Location({
    required this.locationName,
    required this.locationLatitude,
    required this.locationLongitude,
  });

  @override
  List<Object?> get props => [
        locationName,
        locationLatitude,
        locationLongitude,
      ];
}
