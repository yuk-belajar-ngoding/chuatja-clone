import 'package:equatable/equatable.dart';

class WeatherForecastHour extends Equatable {
  final int timeEpoch;
  final double temperatureCelsius;
  final String conditionIcon;
  final String windDirection;
  final int humidity;
  final double uvIndex;

  const WeatherForecastHour({
    required this.timeEpoch,
    required this.temperatureCelsius,
    required this.conditionIcon,
    required this.windDirection,
    required this.humidity,
    required this.uvIndex,
  });

  @override
  List<Object?> get props => [
        timeEpoch,
        temperatureCelsius,
        conditionIcon,
        windDirection,
        humidity,
        uvIndex,
      ];
}
