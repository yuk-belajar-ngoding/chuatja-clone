import 'package:equatable/equatable.dart';

class WeatherCurrent extends Equatable {
  final String lastUpdatedTime;
  final double temperatureCelsius;
  final int isDay; // 1 -> Day mode, 0 -> Night mode
  final String conditionText;
  final String conditionIcon;

  const WeatherCurrent({
    required this.lastUpdatedTime,
    required this.temperatureCelsius,
    required this.isDay,
    required this.conditionText,
    required this.conditionIcon,
  });

  @override
  List<Object?> get props => [
        lastUpdatedTime,
        temperatureCelsius,
        isDay,
        conditionText,
        conditionIcon,
      ];
}
