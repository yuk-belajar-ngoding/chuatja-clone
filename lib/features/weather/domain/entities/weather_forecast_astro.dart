import 'package:equatable/equatable.dart';

class WeatherForecastAstro extends Equatable {
  final String astroSunriseTime;
  final String astroSunsetTime;
  final String astroMoonriseTime;
  final String astroMoonsetTime;

  const WeatherForecastAstro({
    required this.astroSunriseTime,
    required this.astroSunsetTime,
    required this.astroMoonriseTime,
    required this.astroMoonsetTime,
  });

  @override
  List<Object?> get props => [
        astroSunriseTime,
        astroSunsetTime,
        astroMoonriseTime,
        astroMoonsetTime,
      ];
}
