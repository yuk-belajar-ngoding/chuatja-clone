import 'package:chuatjaclone/features/weather/domain/entities/weather_forecast_astro.dart';
import 'package:chuatjaclone/features/weather/domain/entities/weather_forecast_hour.dart';
import 'package:equatable/equatable.dart';

class WeatherForecast extends Equatable {
  final WeatherForecastAstro astro;
  final WeatherForecastHour hour;

  const WeatherForecast({
    required this.astro,
    required this.hour,
  });

  @override
  List<Object?> get props => [astro, hour];
}
