import 'package:chuatjaclone/core/injection_container.dart';
import 'package:chuatjaclone/features/weather/presentation/bloc/get_weather/get_weather_bloc.dart';
import 'package:chuatjaclone/features/weather/presentation/screens/widgets/svg_asset_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => HomeScreenState();
}

class HomeScreenState extends State<HomeScreen> {
  late final GetWeatherBloc weatherBloc;
  bool isLoading = false;
  late final String backgroundImage;

  final TextEditingController _searchController = TextEditingController();
  late String inputStr;
  final FocusNode _searchFocus = FocusNode();

  @override
  void initState() {
    weatherBloc = getIt<GetWeatherBloc>();
    weatherBloc.add(GetWeatherInit());
    backgroundImage = 'assets/images/city-background.svg';

    super.initState();

    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      weatherBloc.add(const GetWeather());
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      // child: Stack(
      //   children: [
      //     Scaffold(
      //       body: Center(
      //         child: Column(
      //           children: [
      //             _buildContent(context),
      //           ],
      //         ),
      //       ),
      //     ),
      //     Center(
      //       child: SvgAssetImage(
      //         assetName: backgroundImage,
      //         color: Theme.of(context).colorScheme.primary,
      //         height: MediaQuery.of(context).size.height / 5,
      //         width: MediaQuery.of(context).size.width / 5,
      //         fit: BoxFit.cover,
      //       ),
      //     ),
      //   ],
      // ),

      child: Scaffold(
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                child: _buildSearchInput(),
              ),
              const SizedBox(height: 10),
              Divider(color: Theme.of(context).colorScheme.primary),
              const SizedBox(height: 10),
              Expanded(
                child: Container(
                  height: 100,
                  color: Colors.green,
                ),
              ),
              _buildContent(context),
              Expanded(
                child: Container(
                  height: 100,
                  color: Colors.green,
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                height: 100,
                color: Colors.green,
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                height: 100,
                color: Colors.green,
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                height: 100,
                color: Colors.green,
              ),
              SizedBox(
                height: 10,
              ),
              Expanded(
                child: Container(
                  height: 10000,
                  color: Colors.yellow,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  BlocProvider<GetWeatherBloc> _buildContent(BuildContext context) {
    return BlocProvider(
      create: (context) => weatherBloc,
      child: Center(
        child: SizedBox(
          child: BlocBuilder<GetWeatherBloc, GetWeatherState>(
            builder: (context, state) {
              if (state is GetWeatherLoaded) {
                isLoading = false;
                return Column(
                  children: [
                    Text("City: ${state.weather.location.locationName}"),
                    const SizedBox(height: 25),
                    Text(
                        "Time: ${state.weather.weatherCurrent.lastUpdatedTime}"),
                    const SizedBox(height: 25),
                    Text("Day/Night: ${state.weather.weatherCurrent.isDay}"),
                  ],
                );
              } else if (state is GetWeatherError) {
                return Column(
                  children: [
                    Text(
                      state.message,
                      style: const TextStyle(fontSize: 24),
                      textAlign: TextAlign.center,
                    ),
                  ],
                );
              }
              isLoading = true;
              return const Center(child: CircularProgressIndicator());
            },
          ),
        ),
      ),
    );
  }

  void dispatchGetWeather() {
    isLoading = true;
    if (inputStr.isNotEmpty) {
      weatherBloc.add(GetWeather(searchKeyword: inputStr));
    } else {
      weatherBloc.add(const GetWeather());
    }
  }

  Widget _buildSearchInput() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: TextFormField(
        controller: _searchController,
        textInputAction: TextInputAction.search,
        decoration: InputDecoration(
          prefixIcon: const Icon(Icons.search),
          labelText: 'Which location weather are you looking for?',
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10.0),
            borderSide: BorderSide(
              color: Theme.of(context).colorScheme.primary,
              width: 2,
            ),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10.0),
            borderSide: BorderSide(
              color: Theme.of(context).colorScheme.primary,
              width: 3,
            ),
          ),
          filled: true,
          fillColor: const Color(0xFFF2F2F2),
        ),
        focusNode: _searchFocus,
        onChanged: (value) {
          setState(() {
            inputStr = value;
          });
        },
        onFieldSubmitted: (_) {
          dispatchGetWeather();
        },
      ),
    );
  }
}
