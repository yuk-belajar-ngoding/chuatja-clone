import 'package:chuatjaclone/core/error/failure.dart';
import 'package:chuatjaclone/features/weather/domain/entities/weather.dart';
import 'package:chuatjaclone/features/weather/domain/usecases/get_weather_usecase.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';

part 'get_weather_event.dart';
part 'get_weather_state.dart';

@injectable
class GetWeatherBloc extends Bloc<GetWeatherEvent, GetWeatherState> {
  final GetWeatherUsecase getWeatherUsecase;
  GetWeatherBloc({required this.getWeatherUsecase})
      : super(GetWeatherInitializing()) {
    on<GetWeatherInit>((event, emit) async => emit(GetWeatherLoading()));

    on<GetWeather>((event, emit) async {
      WeatherParams params = WeatherParams();

      params.searchKeyword = 'Jakarta';
      final failureOrWeather = await getWeatherUsecase(params);
      emit(
        failureOrWeather!.fold(
          (failure) => GetWeatherError(
            message: _mapFailureToMessage(failure),
          ),
          (weatherData) => GetWeatherLoaded(weather: weatherData!),
        ),
      );
    });
  }

  String _mapFailureToMessage(Failure failure) {
    switch (failure.runtimeType) {
      default:
        return defaultFailureMessage;
    }
  }
}
