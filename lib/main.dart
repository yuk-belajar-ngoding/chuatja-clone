import 'package:chuatjaclone/core/chuatja_app.dart';
import 'package:chuatjaclone/core/injection_container.dart';
import 'package:flutter/material.dart';

void main() async {
  await configureDependencies();
  runApp(const ChuatjaApp());
}
